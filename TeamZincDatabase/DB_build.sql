create table if not exists users (
 	id int unsigned auto_increment primary key,
     userName varchar(15) not null unique,
     email VARCHAR(50) not null unique,
     userPassword VARCHAR(20) not null,
     reg_date timestamp)ENGINE=INNODB;
     
create table if not exists stats (
	userid int unsigned not null,
	gamesPlayed int unsigned not null default 0,
    gamesWon int unsigned not null default 0,
    score int unsigned not null default 0,
	primary key (userid),
    foreign key (userid) references users(id)
	on delete restrict
    on update cascade 
)ENGINE=INNODB;

insert into users values 
	(default, "JamesWang", "caibi@gmail.com", "111111", now()),
    (default, "LiamS", "liam@outlook.com", "111111", now()),
    (default, "TomSong", "toms@hotmail.com", "111111", now()),
    (default, "LawrenceLin", "lawrence@qq.com", "111111", now()),
    (default, "WayneYang", "shadiao@163.com", "111111", now());

insert into stats (userid) values (1),(2),(3),(4),(5);
