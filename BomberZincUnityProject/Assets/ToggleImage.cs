﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleImage : MonoBehaviour {

	public void toggleImage() {
        RawImage r = GetComponent<RawImage>();
        if (r.enabled)
        {
            r.enabled = false;
        } else
        {
            r.enabled = true;
        }
    }
}
