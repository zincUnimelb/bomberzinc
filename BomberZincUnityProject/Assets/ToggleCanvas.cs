﻿using UnityEngine;
using System.Collections;

public class ToggleCanvas : MonoBehaviour {

	public void toggleCanvas()
    {
        Canvas c = GetComponent<Canvas>();
        if (c.enabled)
        {
            c.enabled = false;
        } else
        {
            c.enabled = true;
        }

    }
}
