﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System.Collections.Generic;

public class AccountCheckerTest {

	[Test]
	public void CheckEmail()
	{

		List<string> validEmailList = new List<string>();
		validEmailList.Add ("apple@gmail.com");
		validEmailList.Add ("banana@dals.co");
		validEmailList.Add ("lala@ls.co");
		validEmailList.Add ("ca@dals.co");

		List<string> invalidEmailList = new List<string> ();
		invalidEmailList.Add ("apple");
		invalidEmailList.Add ("apple.com.au");
		invalidEmailList.Add ("apple@me");
		invalidEmailList.Add ("apple@me.o");
		invalidEmailList.Add ("apple@a.a");

		// Assert true
		foreach (string s in validEmailList) {
			Assert.True (AccountChecker.Email_check(s));
		}

		// Assert false
		foreach (string s in invalidEmailList) {
			Assert.False (AccountChecker.Email_check(s));
		}
	}
}
