﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class MyJsonAgentTest {

	[Test]
	public void EditorTest()
	{
		MyJsonAgent mj = new MyJsonAgent ("Test");
		mj.addPair ("Title", "title");
		mj.addPair ("Value", "value");
		string expectedOutput = "{\"method\":\"Test\",\"numFields\":\"2\",\"Title\":\"title\",\"Value\":\"value\"}";
		Assert.AreEqual (expectedOutput, mj.getJson());
	}
}
