﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class LoginSessionTest {

	[Test]
	public void EditorTest()
	{
		// Test for logging in
		LoginSession session = LoginSession.Instance;
		session.login ( 1, "James", "caibi@gmail.com");
		Assert.AreEqual (session.Userid, 1);
		Assert.AreEqual (session.UserName, "James");
		Assert.AreEqual (session.Email, "caibi@gmail.com");
		Assert.True (session.IsLoggedIn);

		// Test for logging out
		session.logout();
		Assert.AreEqual (session.Userid, -1);
		Assert.AreEqual (session.UserName, null);
		Assert.AreEqual (session.Email, null);
		Assert.False (session.IsLoggedIn);

	}
}
