﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class UserDataTest {

	[Test]
	public void EditorTest()
	{
		LoginSession session = LoginSession.Instance;
		session.login (1, "JamesWang", "caibi@gmail.com");
		UserData userData = new UserData ();
		int gamesPlayed_1 = userData.getStats("gamesPlayed");
		int gamesWon_1 = userData.getStats ("gamesWon");
		int score_1 = userData.getStats ("score");

		userData.updateStats ("gamesPlayed", 1);
		userData.updateStats ("gamesWon", 1);
		userData.updateStats ("score", 1);

		int gamesPlayed_2 = userData.getStats("gamesPlayed");
		int gamesWon_2 = userData.getStats ("gamesWon");
		int score_2 = userData.getStats ("score");

		Assert.AreEqual (gamesPlayed_1 + 1, gamesPlayed_2);
		Assert.AreEqual (gamesWon_1 + 1, gamesWon_2);
		Assert.AreEqual (score_1 + 1, score_2);
	}
}
