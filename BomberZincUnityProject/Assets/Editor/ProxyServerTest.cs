﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using MiniJSON;

public class ProxyServerTest {

	[Test]
	public void LoginTest()
	{
        ProxyServer ps = new ProxyServer();
        ps.connect();
        MyJsonAgent mj = new MyJsonAgent("Login");
        mj.addPair("Email", "caibi@gmail.com");
        mj.addPair("Password", "111111");
        ps.send(mj.getJson());
        string result = ps.receive();
		string expectedOutput = "{\"method\":\"LoginResult\",\"numFields\":\"4\",\"Result\":\"Success\",\"UserID\":\"1\",\"Email\":\"caibi@gmail.com\",\"Username\":\"JamesWang\"}";
		Assert.AreEqual (expectedOutput, result);
	}
}
