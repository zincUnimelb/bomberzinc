﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System;
using System.Collections.Generic;

/// <summary>
/// A class solely for testing the player, see Unity Test Tools.
/// </summary>
public class PlayerTesting : MonoBehaviour
{

    private GameController controller;
    private int currentYLocation;
    private int currentXLocation;

    private bool paused = false;
    private bool replaying = false;
    private int replayIndex = 0;
    private List<int> yLocations = new List<int>();
    private List<int> xLocations = new List<int>();

    private bool isMoving = false;
    private float fracJourney = 0;
    private Vector3 newPos;

    void Start()
    {
        currentXLocation = Mathf.RoundToInt(transform.position.x);
        currentYLocation = Mathf.RoundToInt(transform.position.z);
        controller = FindObjectOfType<GameController>();
        enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

        //Randomly assigning inputs.
        CrossPlatformInputManager.SetAxis("Horizontal", UnityEngine.Random.Range(-1f, 1f));
        CrossPlatformInputManager.SetAxis("Vertical", UnityEngine.Random.Range(-1f, 1f));
        CrossPlatformInputManager.SetAxis("Fire1", UnityEngine.Random.Range(-1f, 1f));

        enabled = true;
        if (paused)
        {
            // Do nothing.
        }
        else if (isMoving)
        {
            fracJourney += 10f * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, new Vector3(currentXLocation, 0, currentYLocation), fracJourney);
            if (fracJourney >= 1)
            {
                isMoving = false;
                fracJourney = 0;
            }
        }
        else if (replaying)
        {
            if (currentXLocation != xLocations[replayIndex] || currentYLocation != yLocations[replayIndex])
            {
                currentXLocation = xLocations[replayIndex];
                currentYLocation = yLocations[replayIndex];
                newPos = new Vector3(currentXLocation, 0, currentYLocation);
                isMoving = true;
            }
            replayIndex++;
            if (replayIndex > xLocations.Count)
            {
                replayIndex = 0;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.D) || CrossPlatformInputManager.GetAxis("Horizontal") > 0)
            {
                if (controller.isEmpty(currentXLocation + 1, currentYLocation))
                {
                    currentXLocation += 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    isMoving = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.A) || CrossPlatformInputManager.GetAxis("Horizontal") < 0)
            {
                if (controller.isEmpty(currentXLocation - 1, currentYLocation))
                {
                    currentXLocation -= 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    isMoving = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.W) || CrossPlatformInputManager.GetAxis("Vertical") > 0)
            {
                if (controller.isEmpty(currentXLocation, currentYLocation + 1))
                {
                    currentYLocation += 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    isMoving = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.S) || CrossPlatformInputManager.GetAxis("Vertical") < 0)
            {
                if (controller.isEmpty(currentXLocation, currentYLocation - 1))
                {
                    currentYLocation -= 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    isMoving = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetAxis("Fire1") > 0)
            {
                CmdDropBomb();
            }
            xLocations.Add(currentXLocation);
            yLocations.Add(currentYLocation);
        }
    }
    /// <summary>
    /// Called to toggle the pause variable of this class.
    /// </summary>
    public void togglePause()
    {
        if (paused)
        {
            paused = false;
        }
        else
        {
            paused = true;
        }
    }
    /// <summary>
    /// Called when the player presses the bomb button. 
    /// </summary>
    private void CmdDropBomb()
    {
        int x = (int)Math.Round(this.transform.position.x);
        int y = (int)Math.Round(this.transform.position.z);

        //Check if there something there already.
        if (controller.map_items[x, y] == (int)GameController.MapItems.Empty)
        {
            GameObject bomb = controller.placeBomb(x, y);
            controller.bombs.Add(bomb);
            //NetworkServer.Spawn(bomb);
            controller.map_items[x, y] = (int)GameController.MapItems.Bomb;
            Debug.Log("Bomb placed!");

        }
        else
        {
            Debug.Log("Something already here.");
        }
    }
    /// <summary>
    /// This is called to toggle the replay variable in this class.
    /// </summary>
    public void toggleReplay()
    {
        if (replaying)
        {
            replaying = false;
        }
        else
        {
            replaying = true;
            replayIndex = 0;
        }
    }

}
