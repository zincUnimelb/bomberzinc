﻿// The way of smoothly syncing each player's potation 
//is learned from https://forum.unity3d.com/threads/unity-5-unet-multiplayer-tutorials-making-a-basic-survival-co-op.325692/  
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Rotation_sync : NetworkBehaviour {

    //Sync pos for both client and server
    [SyncVar]
    private Quaternion playerRotation;

    [SerializeField]
    public Transform myTransform;
    public float lerpRate = 15;

    // Update is called once per frame
    void FixedUpdate () {
        TransmitRotation();
        LerpRotation();
	
	}

    //get the opponent's pos
    void LerpRotation()
    {
        if (!isLocalPlayer)
        {
            myTransform.localRotation = Quaternion.Lerp(myTransform.localRotation, playerRotation, Time.deltaTime * lerpRate);

        }
    }

    //update the pos on the server side
    [Command]
    void CmdProvedRotationToServr(Quaternion pRot)
    {
        playerRotation = pRot;
    }

    //transimit the self pos to the server
    [ClientCallback]
    void TransmitRotation()
    {
        if (isLocalPlayer)
        {
            CmdProvedRotationToServr(myTransform.localRotation);
        }
    }
}
