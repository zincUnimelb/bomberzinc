﻿using UnityEngine;
using UnityEngine.UI;

public class FetchUserStats : MonoBehaviour
{

    public Text username;
    public Text email;
    public Text gamesWon;
    public Text gamesPlayed;
    public Text score;

    // Use this for initialization
    void Start()
    {

        // read from session logs
        LoginSession loginSession = LoginSession.Instance;
        username.text = loginSession.UserName;
        email.text = loginSession.Email;

        // Fetch user details from the server.
        UserData userData = new UserData();
        gamesWon.text = userData.getStats("gamesWon").ToString();
        gamesPlayed.text = userData.getStats("gamesPlayed").ToString();
        score.text = userData.getStats("score").ToString();

    }
}
