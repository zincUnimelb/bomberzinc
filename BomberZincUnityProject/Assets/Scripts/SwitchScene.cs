﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class SwitchScene : MonoBehaviour {

	public void switchScene(string sceneName){
        if(sceneName.Equals("Matchmaking"))
        {
            MMHUD.showGUI = true;
        }
		SceneManager.LoadScene (sceneName);
	}
}
