﻿using UnityEngine;
using System.Collections;

public sealed class LoginSession : MonoBehaviour {

	// create an instance for singleton
	private static readonly LoginSession instance = new LoginSession();

	// make constructor private
	private LoginSession(){}

	// getter
	public static LoginSession Instance
	{
		get 
		{
			return instance; 
		}
	}

	private int userid = -1;
	private string userName = null;
	private string email = null;
	private bool loggedIn = false;

	// do log in
	public void login(int id, string userName, string email){
		this.userid = id;
		this.userName = userName;
		this.email = email;
		loggedIn = true;
	}

	// do log out
	public void logout(){
		userid = -1;
		userName = null;
		email = null;
		loggedIn = false;
	}


	// getters
	public int Userid {
		get { return userid;}
	}

	public string UserName {
		get { return userName; }
	}

	public string Email{
		get { return email; }
	}

	public bool IsLoggedIn {
		get { return loggedIn; }
	}


	public void printSession(){
		Debug.Log ("id: " + userid);
		Debug.Log ("username: " + userName);
		Debug.Log ("email: " + email);
	}
}
