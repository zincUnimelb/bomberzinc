﻿using UnityEngine;
using System;
using System.Collections;

public class BombScript : MonoBehaviour {

    public float scaleIncreaseFactor = 0.05f;
    [Tooltip("Time until the bomb explodes.")]
    public float bombTimer = 4f;
    public int bombRadius = 4;

    private Vector3 initialSize;
    private DateTime initial;
    private  GameController mapController { get; set; }

    // Use this for initialization
    void Start () {
        mapController = GameObject.FindObjectOfType<GameController>();

        initialSize = transform.localScale;
        initial = DateTime.Now;
        StartCoroutine(explodeBomb(bombTimer));
    }
	
	// Update is called once per frame
	void Update () {
        float timeDifference = (float) (DateTime.Now - initial).TotalMilliseconds;
        float scaler = (float) Math.Pow(timeDifference/1000, 3) * scaleIncreaseFactor + 1;
        transform.localScale = initialSize * scaler;
	}

    IEnumerator explodeBomb(float time)
    {
        yield return new WaitForSeconds(time);

        Debug.Log("Bomb exploded.");
        mapController.bombExploded((int)transform.position.x, (int)transform.position.z, bombRadius);
        Destroy(gameObject);
    }
}
