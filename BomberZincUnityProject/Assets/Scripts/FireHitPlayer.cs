﻿using UnityEngine;

/// <summary>
/// This class checks if fire has hit the player.
/// </summary>
public class FireHitPlayer : MonoBehaviour {

    /// <summary>
    /// This method is called by the (trigger enabled) collider that this player
    /// has collided with.
    /// </summary>
    /// <param name="other">The collider the player has collided with.</param>
    void OnTriggerEnter(Collider other)
    {
        GameObject player = other.gameObject;
        PlayerHealth health = player.GetComponent<PlayerHealth>();
        
        if(health!=null)
        {
            //Take 100 damage killing the player instantly.
            health.TakeDamage(100);
        }

    }
}
