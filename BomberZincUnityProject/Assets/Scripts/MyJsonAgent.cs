﻿using UnityEngine;
using System.Collections;

public class MyJsonAgent : MonoBehaviour {

	private string method;
	private string jsonBody;
	private int numFields;

	public MyJsonAgent (string method){
		this.method = method;
		numFields = 0;
		jsonBody = "";
	}

	public void addPair (string field, string value){
		jsonBody += ",\"" + field + "\":\"" + value + "\"";
		numFields++;
	}

	public void addPair (string field, int value){
		jsonBody += ",\"" + field + "\":\"" + value + "\"";
		numFields++;
	}

	public void addPair (string field, bool value){
		jsonBody += ",\"" + field + "\":\"" + value + "\"";
		numFields++;
	}

	// Return formatted Json String.
	public string getJson(){
		return "{" + getJsonHead() + jsonBody + "}";
	}

	private string getJsonHead(){
		string jh = "\"method\":\"" + method + "\",\"numFields\":\"" + numFields + "\"";
		return jh;
	}


}
