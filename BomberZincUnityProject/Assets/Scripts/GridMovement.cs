﻿using UnityEngine;
using System.Collections;

public class GridMovement : MonoBehaviour {

    public float speed = 2f;

    private GameController mapController;
    private int xPos = 0;
    private int yPos = 0;

    // Use this for initialization
    void Start () {
        xPos = (int)transform.position.x;
        yPos = (int) transform.position.z;
        mapController = GameObject.FindObjectOfType<GameController>();
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.D) && atGridPosition())
        {
            int xNew = xPos + 1;
            if (mapController.isEmpty(xNew, yPos))
            {
                xPos = xNew;
            }
        }
        else if (Input.GetKey(KeyCode.A) && atGridPosition())
        {
            int xNew = xPos - 1;
            if (mapController.isEmpty(xNew, yPos))
            {
                xPos = xNew;
            }
        }
        else if (Input.GetKey(KeyCode.W) && atGridPosition())
        {
            int yNew = yPos + 1;
            if (mapController.isEmpty(xPos, yNew))
            {
                yPos = yNew;
            }
        }
        else if (Input.GetKey(KeyCode.S) && atGridPosition())
        {
            int yNew = yPos - 1;
            if (mapController.isEmpty(xPos, yNew))
            {
                yPos = yNew;
            }
        }

        Vector3 newPosition = new Vector3(xPos, 0, yPos);
        transform.position = Vector3.MoveTowards(transform.position, newPosition, Time.deltaTime * speed);

    }

    private bool atGridPosition()
    {
        if (yPos == Mathf.Round(transform.position.z) && xPos == Mathf.Round(transform.position.x))
        {
            return true;
        }
        return false;
    }
}
