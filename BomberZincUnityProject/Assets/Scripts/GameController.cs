﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameController : NetworkBehaviour
{
    [Header("Map Generator Settings:")]
    public int mapSize;
    [Tooltip("Percentage of the map which should be walls.")]
    public float percentWalls;

    [Header("Game Prefabs:")]
    public GameObject mapWallPrefab;
    public GameObject mapSurroundPrefab;
    public GameObject mapEmptyPrefab;
    public GameObject playerPrefab;
    public GameObject bombPrefab;
    public GameObject firePrefab;

    private int[,] map;
    public int[,] map_items;
    public enum MapCell { Empty, Wall };
    public enum MapItems { Empty, Bomb };

    private List<GameObject> players = new List<GameObject>();
    public List<GameObject> bombs;
    private List<GameObject> walls;

    // Use this for initialization
    void Start()
    {

        bombs = new List<GameObject>();
        walls = new List<GameObject>();

        map = new int[mapSize, mapSize];
        map_items = new int[mapSize, mapSize];
        //generateMap(percentWalls);
        //printMap();
        generateFixedMap();
        instantiateMap();
        instantiatePlayer();
    }

    /// <summary>
    /// This is called once every frame.
    /// </summary>
    void Update()
    {
      
    }

    /// <summary>
    /// Generates a basic map. We need a much more sofisticated algorithm than this.
    /// </summary>
    /// <param name="percentWalls">Percentage of cells which are walls.</param>
    public void generateMap(float percentWalls)
    {
        for (int x = 0; x < mapSize; x++)
        {
            for (int y = 0; y < mapSize; y++)
            {
                map[x, y] = (int)MapCell.Empty;
                map_items[x, y] = (int)MapItems.Empty;
            }
        }

        int numWallsToGenerate = (int)Math.Floor(percentWalls / 100 * (mapSize * mapSize));

        while (numWallsToGenerate > 0)
        {
            int x = UnityEngine.Random.Range(0, mapSize - 1);
            int y = UnityEngine.Random.Range(0, mapSize - 1);
            map[x, y] = (int)MapCell.Wall;
            numWallsToGenerate--;
        }

    }
    /// <summary>
    /// controlling the replay menu
    /// </summary>
    public void toggleReplay()
    {
        Debug.Log("GameController toggleReplay()");
        foreach (Player p in FindObjectsOfType<Player>())
        {
            p.toggleReplay();
        }
    }

    public void togglePause()
    {
        Debug.Log("GameController togglePause()");
        foreach (Player p in FindObjectsOfType<Player>())
        {
            p.togglePause();
        }
    }

    public void quitToMenu()
    {
        SceneManager.LoadScene("Matchmaking");
        MMHUD.showGUI = true;
    }

    /// <summary>
    /// for debugging the map
    /// </summary>
    public void printMap()
    {
        for (int x = 0; x < mapSize; x++)
        {
            string logLine = "";
            for (int y = 0; y < mapSize; y++)
            {
                logLine += map[x, y] + " ";
            }
            Debug.Log(logLine);
        }
    }

    //Creates the map in the Unity space.
    public void instantiateMap()
    {
        GameObject wall;
        GameObject ground;
        for (int x = 0; x < mapSize; x++)
        {
            for (int y = 0; y < mapSize; y++)
            {
                ground = (GameObject)Instantiate(mapEmptyPrefab, new Vector3(x, -0.5f, y), Quaternion.identity);
                ground.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f); if (map[x, y] == (int)MapCell.Wall)
                {
                    wall = (GameObject)Instantiate(mapWallPrefab, new Vector3(x, 0, y), Quaternion.identity);
                    walls.Add(wall);
                }
            }
            wall = (GameObject)Instantiate(mapSurroundPrefab, new Vector3(x, 0, -1f), Quaternion.identity);
            wall = (GameObject)Instantiate(mapSurroundPrefab, new Vector3(x, 0, mapSize), Quaternion.identity);
        }
        for (int y = -1; y <= mapSize; y++)
        {
            wall = (GameObject)Instantiate(mapSurroundPrefab, new Vector3(-1f, 0, y), Quaternion.identity);
            wall = (GameObject)Instantiate(mapSurroundPrefab, new Vector3(mapSize, 0, y), Quaternion.identity);
        }
    }

    private void instantiatePlayer()
    {
        //Make sure there is space around the player.
        removeWall(0, 0);
        removeWall(0, 1);
        removeWall(1, 0);
        //player = (GameObject)Instantiate(playerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
    }

    private bool removeWall(int x, int y)
    {
        foreach (GameObject wall in walls)
        {
            if ((int) wall.transform.position.x == x && (int) wall.transform.position.z == y)
            {
                map[x, y] = (int) MapCell.Empty;
                walls.Remove(wall);
                Destroy(wall);
                Debug.Log("Wall removed at: " + x + "," + y);
                return true;
            }
        }
        return false;
    }

    //Returns true if the player is allowed to move to this map position.
    public bool isEmpty(int x, int y)
    {
        if (!onMap(x, y))
        {
            return false;
        }

        if (map[x, y] == (int)MapCell.Empty)
        {
            return true;
        }
        return false;
    }

    private bool onMap(int x, int y)
    {
        if (x < 0 || y < 0 || x >= mapSize || y >= mapSize)
        {
            return false;
        }
        return true;
    }

    public GameObject placeBomb(int x, int y)
    {
        GameObject bomb = Instantiate(bombPrefab);
        bomb.transform.position += new Vector3(x, -0.6f, y);
        return bomb;
    }

    /// <summary>
    /// Places a fire prefab at the x,y location.
    /// </summary>
    /// <param name="dir">True means left/right, false means up/down.</param>
    private void placeFire(int x, int y, bool dir)
    {
        //if (!onMap(x, y)) return;
        Quaternion rotation = Quaternion.identity;
        if (dir)
        {
            rotation = Quaternion.AngleAxis(90, Vector3.up);
        }
        GameObject fire = (GameObject) Instantiate(firePrefab, new Vector3(x, 0, y), rotation);
        fire.transform.localScale = new Vector3(0.6f, 0.6f, 1);
    }
    
    /// <summary>
    /// and the animation of the bomb explosion
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="bombRadius"></param>
    public void bombExploded(int x, int y, int bombRadius)
    {
        map_items[x, y] = (int) MapCell.Empty;
        placeFire(x, y, true);
        placeFire(x, y, false);
        for (int u = x + 1; u < (x + bombRadius); u++)
        {
            if (!isEmpty(u, y))
            {
                if (onMap(u, y))
                {
                    removeWall(u, y);
                    placeFire(u, y, true);
                }
                break;
            } else
            {
                placeFire(u, y, true);
            }
        }
        for (int u = x - 1; u > (x - bombRadius); u--)
        {
            if (!isEmpty(u, y))
            {
                if (onMap(u, y))
                {
                    removeWall(u, y);
                    placeFire(u, y, true);
                }
                break;
            }
            else
            {
                placeFire(u, y, true);
            }
        }
        for (int u = y + 1; u < (y + bombRadius); u++)
        {
            if (!isEmpty(x, u))
            {
                if (onMap(x, u))
                {
                    removeWall(x, u);
                    placeFire(x, u, false);
                }
                break;
            }
            else
            {
                placeFire(x, u, false);
            }
        }
        for (int u = y - 1; u > (y - bombRadius); u--)
        {
            if (!isEmpty(x, u))
            {
                if (onMap(x, u))
                {
                    removeWall(x, u);
                    placeFire(x, u, false);
                }
                break;
            }
            else
            {
                placeFire(x, u, false);
            }
        }
    }

    /// <summary>
    /// Generate the prefixed map
    /// </summary>
    public void generateFixedMap()
    {

        for (int x = 0; x < 10; x++)
        {
            for (int y = 0; y < 10; y++)
            {
                map[x, y] = (int)MapCell.Empty;
                map_items[x, y] = (int)MapItems.Empty;
            }
        }

        map[1, 1] = 1;
        map[1, 2] = 1;
        map[1, 3] = 1;
        map[1, 4] = 1;
        map[1, 8] = 1;
        map[2, 1] = 1;
        map[2, 4] = 1;
        map[2, 8] = 1;
        map[3, 1] = 1;
        map[4, 1] = 1;
        map[4, 2] = 1;
        map[5, 7] = 1;
        map[5, 8] = 1;
        map[6, 8] = 1;
        map[7, 1] = 1;
        map[7, 5] = 1;
        map[7, 8] = 1;
        map[8, 1] = 1;
        map[8, 5] = 1;
        map[8, 6] = 1;
        map[8, 7] = 1;
        map[8, 8] = 1;

        map[3, 3] = 1;
        map[3, 4] = 1;
        map[4, 3] = 1;
        map[4, 4] = 1;
        map[5, 5] = 1;
        map[5, 6] = 1;
        map[6, 5] = 1;
        map[6, 6] = 1;
    }

}
 