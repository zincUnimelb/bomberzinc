﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;
//Team Zinc

/// <summary>
/// This class controls the input from the screen canvas and
///   translates these into the CrossPlatformInputManager class.
/// </summary>
public class MobileControls : MonoBehaviour
{

    public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
    public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input
    public string fireAxisName = "Fire1"; // The name given to the fire axis for the cross platform input

    private CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
    private CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input
    private CrossPlatformInputManager.VirtualAxis m_FireVirtualAxis; // Reference to the joystick in the cross platform input

    // Use this for initialization
    void Start()
    {
        m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
        CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);

        m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
        CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);


        if (!CrossPlatformInputManager.AxisExists(fireAxisName))
        {
            m_FireVirtualAxis = new CrossPlatformInputManager.VirtualAxis(fireAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(m_FireVirtualAxis);
        }
        else
        {
            m_FireVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(fireAxisName);
        }

        //Sets the transparency of all children buttons
        foreach (Image i in gameObject.GetComponentsInChildren<Image>())
        {
            Color color = i.color;
            color.a = 0.4f;
            i.color = color;
        }
    }


    public void rightButtonClick()
    {
        m_HorizontalVirtualAxis.Update(1);
        //Debug.Log("Right button click");
    }
    public void leftButtonClick()
    {
        m_HorizontalVirtualAxis.Update(-1);
        //Debug.Log("Left button click");
    }
    public void downButtonClick()
    {
        m_VerticalVirtualAxis.Update(-1);
        //Debug.Log("Down button click");
    }
    public void upButtonClick()
    {
        m_VerticalVirtualAxis.Update(1);
        //Debug.Log("Up button click");
    }
    public void bombButtonClick()
    {
        m_FireVirtualAxis.Update(1);
        //Debug.Log("Bomb button click");
    }
    public void leftRightButtonUnClick()
    {
        m_HorizontalVirtualAxis.Update(0);
    }
    public void upDownButtonUnClick()
    {
        m_VerticalVirtualAxis.Update(0);
    }
    public void bombButtonUnClick()
    {
        m_FireVirtualAxis.Update(0);
        Debug.Log("Bomb button unclick");
    }
    void OnDisable()
    {
        // Remove the joysticks from the cross platform input
        m_HorizontalVirtualAxis.Remove();
        m_VerticalVirtualAxis.Remove();
    }
}
