﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ClearPlaceHolder : MonoBehaviour
{

    public string placeHolder = "";

    /// <summary>
    /// Clears the placeholder text.
    /// </summary>
    /// <param name="inputField">Input Field to clear text.</param>
    public void clearPlaceHolder(InputField inputField)
    {
        if (inputField.text == "")
            inputField.placeholder.GetComponent<Text>().text = placeHolder;
        else
            inputField.placeholder.GetComponent<Text>().text = "";
    }
}
