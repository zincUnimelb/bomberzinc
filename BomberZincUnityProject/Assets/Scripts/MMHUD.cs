﻿//The skeleton of the class come from the Unity Source code, it is adapted to suit the matchmaking for our own game.
using System;
using System.ComponentModel;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine;
#if ENABLE_UNET

/// <summary>
/// Multiplayer controller.
/// </summary>
[AddComponentMenu("Network/NetworkManagerHUD")]
[RequireComponent(typeof(NetworkManager))]
[EditorBrowsable(EditorBrowsableState.Never)]
public class MMHUD : MonoBehaviour
{

    //UI component
    public NetworkManager manager;
    [SerializeField]
    public static bool showGUI;
    [SerializeField]
    public float offsetX;
    [SerializeField]
    public float offsetY;

    public Font font = null;

    // Runtime variable
    bool m_ShowServer;
    private int screenX;
    private int screenY;
    private int boxSizeX;
    private int boxSizeY;

    private UserData userData;
    private int fontSize = 50;

    void Awake()
    {
        //show the GUI when loading into the MM scene
        showGUI = true;
        manager = GetComponent<NetworkManager>();
        screenX = Screen.width;
        screenY = Screen.height;

        boxSizeX = (int)(screenX * 0.3f);
        boxSizeY = (int)(screenY * 0.15f);

        fontSize *= (int)(screenY / 1080f);
    }

    void Update()
    {
        //match making server finished job,don't update
        if (!showGUI)
            return;
        if (!manager.IsClientConnected() && !NetworkServer.active && manager.matchMaker == null)
        {

        }
        if (NetworkServer.active && manager.IsClientConnected())
        {

        }
    }

    void OnGUI()
    {
        if (!showGUI)
            return;

        //Set fonts.
        GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = fontSize;
        GUI.skin.font = font;

        //Set button positions based on screen size.
        int xpos = (int)(offsetX * screenX);
        int ypos = (int)(offsetY * screenY);
        int spacing = (int)(0.17 * screenY);

        bool noConnection = (manager.client == null || manager.client.connection == null ||
                             manager.client.connection.connectionId == -1);

        //First phase, no client connected
        if (!manager.IsClientConnected() && !NetworkServer.active && manager.matchMaker == null)
        {
            if (noConnection)
            {

                if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Practice Range"))
                {
                    Time.timeScale = 1;
                    manager.StartHost();
                    showGUI = false;
                }
                ypos += spacing;

            }
            else
            {
                GUI.Label(new Rect(xpos, ypos, 200, 20), "Connecting to " + manager.networkAddress + ":" + manager.networkPort + "..");
                ypos += spacing;


                if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Cancel Connection Attempt"))
                {
                    manager.StopClient();
                }
            }
        }
        else
        {
            if (NetworkServer.active)
            {
                string serverMsg = "Server: port=" + manager.networkPort;
                if (manager.useWebSockets)
                {
                    serverMsg += " (Using WebSockets)";
                }
                GUI.Label(new Rect(xpos, ypos, 300, 20), serverMsg);
                ypos += spacing;
            }
            if (manager.IsClientConnected())
            {
                GUI.Label(new Rect(xpos, ypos, 300, 20), "Client: address=" + manager.networkAddress + " port=" + manager.networkPort);
                ypos += spacing;
            }
        }

        if (manager.IsClientConnected() && !ClientScene.ready)
        {
            if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Client Ready"))
            {
                ClientScene.Ready(manager.client.connection);

                if (ClientScene.localPlayers.Count == 0)
                {
                    ClientScene.AddPlayer(0);
                }
            }
            ypos += spacing;
        }

        if (NetworkServer.active || manager.IsClientConnected())
        {
            if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Back To MatchMaking"))
            {
                manager.StopHost();
            }
            ypos += spacing;
        }

        //Second phase, matchmaking service is enabled
        if (!NetworkServer.active && !manager.IsClientConnected() && noConnection)
        {
            ypos += 10;

            if (manager.matchMaker == null)
            {
                if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Multiplayer Game"))
                {
                    manager.StartMatchMaker();
                }
                ypos += spacing;

                if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Profile"))
                {
                    SceneManager.LoadScene("Profile");
                    showGUI = false;
                }
                ypos += spacing;
                if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Log Out"))
                {
                    SceneManager.LoadScene("FirstScene");
                    showGUI = false;
                }
                ypos += spacing;

            }
            else
            {
                if (manager.matchInfo == null)
                {
                    if (manager.matches == null)
                    {
                        if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Create Match"))
                        {
                            string matchName;
                            //retrive the user name,and create a match with it.
                            matchName = LoginSession.Instance.UserName;
                            Time.timeScale = 1;
                            manager.matchMaker.CreateMatch(matchName, manager.matchSize, true, "", "", "", 0, 0, manager.OnMatchCreate);
                            showGUI = false;
                        }
                        ypos += spacing;


                        //ypos += 10;

                        if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Find Match"))
                        {
                            manager.matchMaker.ListMatches(0, 20, "", false, 0, 0, manager.OnMatchList);
                        }
                        ypos += spacing;
                    }
                    else
                    {
                        for (int i = 0; i < manager.matches.Count; i++)
                        {
                            var match = manager.matches[i];
                            if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Join Match:" + match.name))
                            {

                                manager.matchName = match.name;
                                //match started,reset the time scale to 1
                                Time.timeScale = 1;
                                //join the selected match
                                manager.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, manager.OnMatchJoined);
                                showGUI = false;
                            }
                            ypos += spacing;
                        }

                        if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Back to Match Menu"))
                        {
                            manager.matches = null;
                        }
                        ypos += spacing;
                    }
                }

                if (GUI.Button(new Rect(xpos, ypos, boxSizeX, boxSizeY), "Back"))
                {
                    manager.StopMatchMaker();
                }
                ypos += spacing;
            }
        }
    }
}
#endif //ENABLE_UNET