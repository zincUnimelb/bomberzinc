﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using System;

// Usage:	1. instantiate the class
//			2. call connect() first checking connection
//			3. call methods as need
//			4. after use, call close() to close connection
public class UserData : MonoBehaviour
{

    public LoginSession loginSession;
    private ProxyServer ps;

    public UserData()
    {
        loginSession = LoginSession.Instance;
    }

    /// <summary>
    /// Tries to connect to the proxy server.
    /// </summary>
    /// <returns></returns>
    private bool connect()
    {

        if (!ps.connect())
            return false;
        else
            return true;
    }

    /// <summary>
    /// Shorcut class to close the proxy server.
    /// </summary>
    private void close()
    {
        ps.close();
    }

    /// <summary>
    /// Retrieves the (int) statistics from the server.
    /// </summary>
    /// <param name="field">Statistic name to retrieve.</param>
    /// <returns>Statistic.</returns>
    public int getStats(string field)
    {

        ps = new ProxyServer();
        connect();

        int value = 0;
        MyJsonAgent mj = new MyJsonAgent("RequestStats");
        mj.addPair("id", loginSession.Userid);
        mj.addPair("field", field);

        // Send query
        ps.send(mj.getJson());

        // Receive response
        string result = ps.receive();
        var dict = Json.Deserialize(result) as Dictionary<string, object>;

        if (dict["Result"].Equals("Success"))
        {
            // Read Information
            value = Convert.ToInt32(dict["value"]);
        }
        else
        {
            Debug.Log("Log In failed.");
        }

        close();
        return value;
    }

    
    /// <summary>
    /// Updates statistics on the server.
    /// </summary>
    /// <param name="field">Statistic to update.</param>
    /// <param name="value">Value to update it to.</param>
    public void updateStats(string field, int value)
    {

        ps = new ProxyServer();
        connect();

        MyJsonAgent mj = new MyJsonAgent("UpdateStats");
        mj.addPair("id", loginSession.Userid);
        mj.addPair("field", field);
        mj.addPair("value", value);

        // Send query
        ps.send(mj.getJson());
        Debug.Log(mj.getJson());
        // Receive login result from server
        string result = ps.receive();
        Debug.Log(result);
        var dict = Json.Deserialize(result) as Dictionary<string, object>;

        if (!dict["Result"].Equals("Success"))
        {

            Debug.Log("update field " + dict["field"] + " failed.");
        }

        close();
    }

}
