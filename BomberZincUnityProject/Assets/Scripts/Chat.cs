﻿//this class is adapted from the online tutorial http://frostybay.com/articles/a-simple-tutorial-with-unity-unet
using UnityEngine;
using UnityEngine.UI;


// An interface for implementing the chat function
public class Chat : MonoBehaviour
{
    //UI component
    [SerializeField]
    private Text container;
    [SerializeField]
    private ScrollRect rect;

    /// <summary>
    /// Called to added a message to the chat dialog.
    /// </summary>
    /// <param name="message">Message to add</param>
    internal void AddMessage(string message)
    {
        container.text += "\n" + message;

        //Scrolls the chat down when adding a message after 100 ms.
        Invoke("ScrollDown", .1f);
    }
    
    public virtual void SendMessage(InputField input) { }

    /// <summary>
    /// This methods scrolls the chat down.
    /// </summary>
    private void ScrollDown()
    {
        if (rect != null)
            rect.verticalScrollbar.value = 0;
    }
}