﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using MiniJSON;
using System;
using UnityEngine.SceneManagement;

public class SignUpHandler : MonoBehaviour {

	public InputField email;
	public InputField password;
	public InputField username;

	private bool showFail = false;
	private string promptMessage = "";
	private float excutedTime = 0.0f, currentTime = 0.0f, timeToWait = 2.0f;

	// TO-DO check validity of email input
	public void doSignUp(){

		// check email format first
		if (!AccountChecker.Email_check (email.text)) {
			excutedTime = Time.time;
			showFail = true;
			promptMessage = "Invalid Email Address.";
			return;
		}

		// connect to server
		ProxyServer ps = new ProxyServer ();
		if (!ps.connect ()) {
			return;
		}

		// Format message to JSON
		MyJsonAgent mj = new MyJsonAgent ("SignUp");
		mj.addPair ("Email", email.text);
		mj.addPair ("Password", password.text);
		mj.addPair ("Username", username.text);

		// Send query
		ps.send (mj.getJson ());

		// Receive sign up result from server
		string result = ps.receive ();
//		Debug.Log (result);
		var dict = Json.Deserialize (result) as Dictionary<string, object>;
		if (dict ["Result"].Equals("Success")) {
			// Read Information
			int login_id = Convert.ToInt32(dict ["UserID"]) ;
			string login_userName = (string) dict ["Username"];
			string login_email = (string) dict ["Email"];

			// Store login information to session
			LoginSession loginSession = LoginSession.Instance;
			loginSession.login (login_id, login_userName, login_email);
//			loginSession.printSession();

			// Switch to Main Scene
			SceneManager.LoadScene ("Matchmaking");
		} else {
			excutedTime = Time.time;
			showFail = true;
			promptMessage = "Sign Up Failed.";
		}
		ps.close ();
	}

	void Update(){
		currentTime = Time.time;
		if (currentTime - excutedTime > timeToWait) {
			showFail = false;
			promptMessage = "";
		}

	}

	void OnGUI() {
		if (showFail) {
			int screenX = Screen.width;
			int screenY = Screen.height;
			int fontSize = (int)(125 * (screenY / 1080f));
			Vector2 boxSize = GUI.skin.label.CalcSize (new GUIContent(promptMessage));
			int boxX = (int)boxSize.x;
			int boxY = (int)boxSize.y;
			GUI.skin.label.alignment = TextAnchor.MiddleCenter;
			GUI.skin.label.fontSize = fontSize;
			GUI.skin.label.wordWrap = false;
			GUI.color = new Color (0f, 0f, 0f, 0.65f);

			GUI.Label (new Rect (screenX / 2 - boxX / 2, screenY / 2 - boxY / 2, boxX, boxY), promptMessage);
		}
	}
}
