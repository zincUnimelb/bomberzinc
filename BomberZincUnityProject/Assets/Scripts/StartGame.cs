﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour { 

    public void startGame()
    {
        Debug.Log("Start Game");
        gameObject.GetComponentInChildren<Text>().text = "Loading...";
        SceneManager.LoadScene(1);
    }
}
