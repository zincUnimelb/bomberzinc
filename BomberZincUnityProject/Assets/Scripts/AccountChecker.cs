﻿using UnityEngine;
using System.Collections;

public class AccountChecker : MonoBehaviour {
    public static bool Email_check(string Email)
    {
      
        int count = 0;
        bool flag1 = false;
        bool flag2 = false;
        int temp = Email.Length;

        //checking if the Email address contains "@";
        if (Email.Contains("@"))
        {
            //checking if there any letter before "@";
            if (1 < Email.IndexOf("@"))
            {
                flag1 = true;
            }
        }

        //checking is there any letter in between "@" and ".";
        int a = Email.IndexOf("@");
        //check is there any letter after ".";
        for (int i = a + 3; i < temp; i++)
        {
            if (Email[i] == '.')
            {
                flag2 = true;
                count = 0;
            }
            count = count + 1;
        }
        if (count < 3)
        {
            flag2 = false;
        }
        //if both flag are true, means there is "@"
        //and the format is right, return true;
        if (flag1 && flag2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public static bool Password_check(string password)
    {
        bool flag = true;
        //check if the password lenth is long enough;
        if (password.Length < 4)
        {
            flag = false;
        }
        //check if the passwore contain capital letter;
        if (!password.Contains(".*[A-Z].*"))
        {
            flag = false;
        }
        //check if the passwore contain lower case letter;
        if (!password.Contains(".*[a-z].*"))
        {
            flag = false;
        }
        //check if the passwore contains interger;
        if (!password.Contains(".*\\d.*"))
        {
            flag = false;
        }
        //return true if pass all test;
        if (flag)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
