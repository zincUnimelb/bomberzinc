﻿//Class for determine whether player is dead
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.UI;


public class PlayerHealth : NetworkBehaviour
{

    // set the player to be killed by one hit
    public const int maxHealth = 100;
    [SyncVar]
    public int currentHealth = maxHealth;

    public void TakeDamage(int amount)
    {

        if (!isServer)
        {
            return;
        }
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            //Debug.Log("Dead!");
            RpcGameOver();
            //CmdGameOver();
        }
    }

    //invoke when the player's health reaches zero
    public void RpcGameOver()
    {


        //get the user data and stop game
        UserData userData = new UserData();
        Time.timeScale = 0;

        //show game over message
        GameObject menu = GameObject.Find("GameOverMenu");
        menu.GetComponent<Canvas>().enabled = true;


        //update player's account stats
        userData.updateStats("gamesPlayed", 1);

        GameObject.Find("diedText").GetComponent<Text>().text = "Try again?";


        //update both player's stats
        if (isLocalPlayer)
        {
            userData.updateStats("score", 1);
        }
        else
        {
            userData.updateStats("gamesWon", 1);
            userData.updateStats("score", 5);
        }
    }

    [Command]
    public void CmdGameOver()
    {
        UserData userData = new UserData();
        Time.timeScale = 0;
        GameObject menu = GameObject.Find("GameOverMenu");
        menu.GetComponent<Canvas>().enabled = true;
        userData.updateStats("gamesPlayed", 1);
        GameObject.Find("diedText").GetComponent<Text>().text = "Try again?";

        if (isLocalPlayer)
        {
            userData.updateStats("score", 1);
        }
        else
        {
            userData.updateStats("gamesWon", 1);
            userData.updateStats("score", 5);

        }
    }

}
