﻿//Class for controlling all player activities,including movement,bomb drop,animation,replay etc.
using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Player : NetworkBehaviour
{
    public Camera playerCamera;
    public AudioListener playerAudio;
    public float animationSpeed = 6f;

    //Private variables
    private Transform dog;
    private GameController controller;
    private Animator animator;
    private int currentYLocation;
    private int currentXLocation;
    private bool paused = false;
    private bool replaying = false;
    private int replayIndex = 0;
    private List<int> yLocations = new List<int>();
    private List<int> xLocations = new List<int>();
    private bool isMoving = false;
    private float fracJourney = 0;
    private Vector3 newPos;
    private float lerpRate = 10f;

    void Start()
    {
        currentXLocation = Mathf.RoundToInt(transform.position.x);
        currentYLocation = Mathf.RoundToInt(transform.position.z);

        controller = FindObjectOfType<GameController>();
        dog = transform.FindChild("walkingdog");
        animator = GetComponentInChildren<Animator>();
        animator.speed = animationSpeed;
        animator.enabled = false;

        if (isLocalPlayer)
        {
            playerCamera.enabled = true;
            playerAudio.enabled = true;
        }
    }


    /// <summary>
    /// Set the local player's color to green.
    /// </summary>
    public override void OnStartLocalPlayer()
    {
        foreach (SkinnedMeshRenderer skinnedMeshR in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            foreach(Material material in skinnedMeshR.materials)
            {
                material.color = Color.green;
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (paused)
        {

        }
        else if (isMoving)
        {
            fracJourney += lerpRate * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, new Vector3(currentXLocation, 0, currentYLocation), fracJourney);
            if (fracJourney >= 1)
            {
                isMoving = false;
                fracJourney = 0;
                animator.enabled = false;
            }
        }
        else if (replaying)
        {
            if (currentXLocation != xLocations[replayIndex] || currentYLocation != yLocations[replayIndex])
            {
                if (currentXLocation < xLocations[replayIndex])
                {
                    dog.localRotation = Quaternion.AngleAxis(180, Vector3.up);
                } else if (currentXLocation > xLocations[replayIndex])
                {
                    dog.localRotation = Quaternion.AngleAxis(0, Vector3.up);
                }
                if (currentYLocation < yLocations[replayIndex])
                {
                    dog.localRotation = Quaternion.AngleAxis(90, Vector3.up);
                }
                else if (currentYLocation > yLocations[replayIndex])
                {
                    dog.localRotation = Quaternion.AngleAxis(270, Vector3.up);
                }

                currentXLocation = xLocations[replayIndex];
                currentYLocation = yLocations[replayIndex];
                newPos = new Vector3(currentXLocation, 0, currentYLocation);
                startMoving();
            }
            replayIndex++;
            if (replayIndex > xLocations.Count)
            {
                replayIndex = 0;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.D) || CrossPlatformInputManager.GetAxis("Horizontal") > 0)
            {
                if (controller.isEmpty(currentXLocation + 1, currentYLocation))
                {
                    currentXLocation += 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    startMoving();
                    dog.localRotation = Quaternion.AngleAxis(180, Vector3.up);
                }
            }
            else if (Input.GetKeyDown(KeyCode.A) || CrossPlatformInputManager.GetAxis("Horizontal") < 0)
            {
                if (controller.isEmpty(currentXLocation - 1, currentYLocation))
                {
                    currentXLocation -= 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    startMoving();
                    dog.localRotation = Quaternion.AngleAxis(0, Vector3.up);
                }
            }
            else if (Input.GetKeyDown(KeyCode.W) || CrossPlatformInputManager.GetAxis("Vertical") > 0)
            {
                if (controller.isEmpty(currentXLocation, currentYLocation + 1))
                {
                    currentYLocation += 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    startMoving();
                    dog.localRotation = Quaternion.AngleAxis(90, Vector3.up);
                }
            }
            else if (Input.GetKeyDown(KeyCode.S) || CrossPlatformInputManager.GetAxis("Vertical") < 0)
            {
                if (controller.isEmpty(currentXLocation, currentYLocation - 1))
                {
                    currentYLocation -= 1;
                    newPos = new Vector3(currentXLocation, 0, currentYLocation);
                    startMoving();
                    dog.localRotation = Quaternion.AngleAxis(270, Vector3.up);
                }
            }
            else if (Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetAxis("Fire1") > 0)
            {
                CmdDropBomb();
            }
            xLocations.Add(currentXLocation);
            yLocations.Add(currentYLocation);
        }
    }

    /// <summary>
    /// Called to start movement of the player. 
    /// </summary>
    private void startMoving()
    {
        isMoving = true;
        animator.enabled = true;
    }

    /// <summary>
    /// Called to toggle the pause variable of this class.
    /// </summary>
    public void togglePause()
    {
        if (paused)
        {
            paused = false;
        }
        else
        {
            paused = true;
        }
    }

    /// <summary>
    /// Called when the player presses the bomb button. 
    /// </summary>
    [Command]
    private void CmdDropBomb()
    {
        int x = (int)Math.Round(this.transform.position.x);
        int y = (int)Math.Round(this.transform.position.z);

        //Check if the location is empty to put a bomb.
        if (controller.map_items[x, y] == (int)GameController.MapItems.Empty)
        {
            GameObject bomb = controller.placeBomb(x, y);
            controller.bombs.Add(bomb);
            NetworkServer.Spawn(bomb);
            controller.map_items[x, y] = (int)GameController.MapItems.Bomb;
            Debug.Log("Bomb placed!");

        }
        else
        {
            //Otherwise do nothing.
            Debug.Log("Something already here.");
        }
    }

    /// <summary>
    /// This is called to toggle the replay variable in this class.
    /// </summary>
    public void toggleReplay()
    {
        if (replaying)
        {
            replaying = false;
        }
        else
        {
            replaying = true;
            replayIndex = 0;
        }
    }

}
