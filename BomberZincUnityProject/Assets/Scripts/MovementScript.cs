﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// An old unused class.
/// </summary>
public class MovementScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float forward = CrossPlatformInputManager.GetAxis("Vertical") * 3 * Time.deltaTime;
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal") * -3 * Time.deltaTime;
        transform.Translate(Vector3.forward * forward);
        transform.Translate(Vector3.left * horizontal);
    }

    public bool leftClicked()
    {
        return true;
    }
}
