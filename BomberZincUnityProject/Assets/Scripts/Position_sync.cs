﻿//Tutorial https://forum.unity3d.com/threads/unity-5-unet-multiplayer-tutorials-making-a-basic-survival-co-op.325692/  
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/// <summary>
/// Smoothly syncing each player's position and rotation.
/// </summary>
public class Position_sync : NetworkBehaviour
{

    //Sync pos for both client and server
    [SyncVar]
    private Vector3 syncPos;

    public Transform myTransform;
    public float lerpRate = 10;

    void FixedUpdate()
    {
        //transmit player's own pos
        TransmitPosition();
        //sync the opponent's pos
        LerpPosition();
    }

    //get the opponent's pos
    void LerpPosition()
    {
        if (!isLocalPlayer)
        {
            myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime * lerpRate);
        }
    }

    //update the pos on the server side
    [Command]
    void CmdProvedPostionToServr(Vector3 pos)
    {
        syncPos = pos;
    }

    //transimit the self pos to the server
    [ClientCallback]
    void TransmitPosition()
    {
        if (isLocalPlayer)
        {
            CmdProvedPostionToServr(myTransform.position);
        }
    }
}
