﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System;


// To connect and communicate with server
// Call connect before sending and receiving message
// Call close after finished.
public class ProxyServer : MonoBehaviour {

	private string ipAddress;
	private int portNumber;
	private IPEndPoint remoteEP;
	private Socket socket;


	public ProxyServer(){
//		this.ipAddress = "127.0.0.1";
//		this.ipAddress = "115.159.24.243";
		this.ipAddress = "115.146.86.130";
		this.portNumber = 3333;
		IPAddress ipAddressParsed = IPAddress.Parse(ipAddress);
		this.remoteEP = new IPEndPoint(ipAddressParsed, portNumber);
		this.socket = new Socket (AddressFamily.InterNetwork, 
			SocketType.Stream, ProtocolType.Tcp );
		
	}


	// Connect to remoteEP
	// TO-DO: Need to check if connected successfully later
	public bool connect(){
		bool connected = true;
		try {
			socket.Connect (remoteEP);
		} catch (SocketException se) {
			Debug.Log("SocketException : " + se.ToString());
			connected = false;
		} catch (Exception e) {
			Debug.Log("Unexpected exception : " + e.ToString());
			connected = false;
		}
		return connected;
	}


	// receiving message
	// return null if no message received
	public string receive(){

		// string to return
		string rcv = null;

		try {

		// receive the length n of message
		byte[] rcvLenBytes = new byte[4];
		socket.Receive(rcvLenBytes);
		int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);

		// receive n bytes of message
		byte[] rcvBytes = new byte[rcvLen];
		socket.Receive(rcvBytes);

		// decode message to string
		rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
		}  catch (SocketException se) {
			Console.WriteLine("SocketException : {0}",se.ToString());
		} catch (Exception e) {
			Console.WriteLine("Unexpected exception : {0}", e.ToString());
		}

		return rcv;
	}


	// sending message
	// return true if message sent successfully,
	// return false if failed sending message
	// TO-DO check if the message sent successfully, later on
	public bool send(String msg){
		bool sendSuccessfully = false;
		try {
			// lenth of msg to be sent
			int toSendLen = System.Text.Encoding.ASCII.GetByteCount(msg);

			// encode message
			byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(msg);
			byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);

			// send length first, then send message
			socket.Send(toSendLenBytes);
			socket.Send(toSendBytes);
			sendSuccessfully = true;
		} catch (SocketException se) {
			Console.WriteLine("SocketException : {0}",se.ToString());
		} catch (Exception e) {
			Console.WriteLine("Unexpected exception : {0}", e.ToString());
		}
		return sendSuccessfully;
	}


	// Close connection
	public void close(){
//		socket.Shutdown (SocketShutdown.Both);
		socket.Close ();
	}
}
