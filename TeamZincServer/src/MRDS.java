/*
 * Created by Lawrence at 1:39 PM 16/09/2016   
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Message Receiving and Dispatching System.
 */
public class MRDS {


    /**
     * Receive message via inputStream
     * Protocol:
     *      1. read a byte[4] integer n which is the length of message
     *      2. read the message of length n
     * @param in the inputStream
     * @return string of message read
     */
    public static String receive(InputStream in) {
        String received = null;
        try {
            byte[] lenBytes = new byte[4];
            in.read(lenBytes, 0, 4);
            // bit wise manipulation
            int len = (lenBytes[3] & 0xff) << 24 | (lenBytes[2] & 0xff) << 16 |
                    (lenBytes[1] & 0xff) << 8 | lenBytes[0] & 0xff;
            byte[] receivedBytes = new byte[len];
            in.read(receivedBytes, 0, len);
            received = new String(receivedBytes, 0, len);
        } catch (IOException e) {
            Log.log("Exception caught when trying to receive message. " + e.getMessage());
        }
        return received;
    }

    /**
     * Send message to client via outputStream
     * @param out outputStream to client
     * @param message message to send
     */
    public static void send(OutputStream out, String message) {
        try {
            byte[] toSendBytes = message.getBytes();
            int toSendLen = toSendBytes.length;
            byte[] toSendLenBytes = new byte[4];
            toSendLenBytes[0] = (byte) (toSendLen & 0xff);
            toSendLenBytes[1] = (byte) (toSendLen >> 8 & 0xff);
            toSendLenBytes[2] = (byte) (toSendLen >> 16 & 0xff);
            toSendLenBytes[3] = (byte) (toSendLen >> 24 & 0xff);
            out.write(toSendLenBytes);
            out.write(toSendBytes);
        } catch (IOException e) {
            Log.log("Exception caught when trying to send message. " + e.getMessage());
        }

    }

}
