/*
 * Created by Lawrence at 7:38 PM 19/09/2016   
 */


public class MyJsonAgent {
    private String title;
    private String jsonBody;
    private int numFields;

    public MyJsonAgent(String title) {
        this.title = title;
        numFields = 0;
        jsonBody = "";
    }

    public void addPair (String field, String value){
        jsonBody += ",\"" + field + "\":\"" + value + "\"";
        numFields++;
    }

    public void addPair (String field, int value){
        jsonBody += ",\"" + field + "\":\"" + value + "\"";
        numFields++;
    }

    public void addPair (String field, Boolean value){
        jsonBody += ",\"" + field + "\":\"" + value + "\"";
        numFields++;
    }

    public String getJson(){
        return "{" + getJsonHead() + jsonBody + "}";
    }

    private String getJsonHead(){
        String jh = "\"method\":\"" + title + "\",\"numFields\":\"" + numFields + "\"";
        return jh;
    }
}
