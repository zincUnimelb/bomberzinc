/*
 * Created by Lawrence at 3:09 PM 13/10/2016   
 */


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;

public class Log {

    static Timestamp timestamp_1 = null;

    public static void log(String message) {
        Timestamp timestamp_2 = new Timestamp(new Date().getTime());
        File log = new File("log.txt");
        try{
            if(log.exists()==false){
                log.createNewFile();
            }
            PrintWriter out = new PrintWriter(new FileWriter(log, true));
            if (timestamp_1 == null || timestamp_2.getTime() - timestamp_1.getTime() > 2000){
                out.append(timestamp_2 + "\n" + "\t" + message + "\n");
            }else {
                out.append("\t" + message + "\n");
            }
            out.close();
        }catch(IOException e){
            System.err.println("Print Log exception. " + e.getMessage());
        }
    }
}
