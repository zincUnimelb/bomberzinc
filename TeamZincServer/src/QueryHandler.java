/*
 * Created by Lawrence at 1:15 PM 16/09/2016   
 */

import java.io.OutputStream;
import java.sql.*;
import org.json.*;


public class QueryHandler {

    //<editor-fold desc="variables to communicate mysql DB & client">
    private OutputStream outputStream;
    private Connection connection;
    //</editor-fold>


    /**
     * Construct QueryHandler
     * @param connection to communicate with mysql DB
     * @param outputStream to send message to client
     */
    public QueryHandler( Connection connection, OutputStream outputStream) {
        this.connection = connection;
        this.outputStream = outputStream;
    }


    /**
     * parse json into query type and variables
     * @param message Json typed message to parse
     */
    public void parseQuery(String message){

        // Get type of query received from client
        JSONObject jsonObject = new JSONObject(message);
        String queryType = jsonObject.getString("method");

        // Task dispatcher
        switch (queryType) {

            case "SignUp":
                if (jsonObject.getInt("numFields") != 3) {
                    MRDS.send(outputStream, "Invalid Request.");
                }
                signUpHandler(jsonObject.getString("Email"),
                        jsonObject.getString("Password"), jsonObject.getString("Username"));
                break;

            case "Login":
                if (jsonObject.getInt("numFields") != 2) {
                    MRDS.send(outputStream, "Invalid Request.");
                }
                loginHandler(jsonObject.getString("Email"), jsonObject.getString("Password"));
                break;

            case "RequestStats":
                if (jsonObject.getInt("numFields") != 2) {
                    MRDS.send(outputStream, "Invalid Request.");
                }
                requestStatsHandler(jsonObject.getString("id"), jsonObject.getString("field"));
                break;

            case "UpdateStats":
                if (jsonObject.getInt("numFields") != 3) {
                    MRDS.send(outputStream, "Invalid Request.");
                }
                updateStatsHandler(jsonObject.getString("id"), jsonObject.getString("field"), jsonObject.getString("value"));
            default:
                MRDS.send(outputStream, "Unknown Request.");
        }

    }


    /**
     * Log user in with email and password
     * @param email
     * @param password
     */
    public void loginHandler(String email, String password) {

        // Logging in query
        String query = "SELECT COUNT(*), id, userName, email FROM users WHERE email = \"" + email + "\" AND userPassword = \"" + password + "\";";

        // format message to be sent to client as Json
        MyJsonAgent myJsonAgent = new MyJsonAgent("LoginResult");

        try {
            // try logging in
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                if (resultSet.getInt(1)==1) {
                    // successfully logged in
                    myJsonAgent.addPair("Result", "Success");
                    myJsonAgent.addPair("UserID", resultSet.getInt("id"));
                    myJsonAgent.addPair("Email", resultSet.getString("email"));
                    myJsonAgent.addPair("Username", resultSet.getString("userName"));
                    MRDS.send(outputStream, myJsonAgent.getJson());
                }
                else {
                    // failed logging in
                    myJsonAgent.addPair("Result", "Fail");
                    MRDS.send(outputStream, myJsonAgent.getJson());
                }
            }
        } catch (java.sql.SQLException e) {
            // failed logging in
            Log.log("Mysql Exception at Login Handler. " + e.getMessage());
            myJsonAgent.addPair("Result", "Fail");
            MRDS.send(outputStream, myJsonAgent.getJson());
        }
    }


    /**
     * Create account with email and password
     * @param email
     * @param password
     */
    public void signUpHandler(String email, String password, String username) {
        // TO-DO check email format and uniqueness

        // signing up query
//        String query = "insert into users values (default, default, \""+password+"\", \""+email+"\");";
        String query = "insert into users values (default, \""+username+"\", \""+email+"\", \""+password+"\", now());";
        boolean result = false;
        MyJsonAgent myJsonAgent = new MyJsonAgent("SignUpResult");
        try {
            // excute query
            Statement statement = connection.createStatement();
            statement.execute(query);

            // successfully inserted values
            myJsonAgent.addPair("Result", "Success");

            // fetch user id
            query = "SELECT * FROM users where email =\"" + email + "\";";
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                myJsonAgent.addPair("UserID", id);

                query = "insert into stats  (userid)  values (" + id + ");";
                statement = connection.createStatement();
                statement.executeUpdate(query);
                myJsonAgent.addPair("Email", resultSet.getString("email"));
                myJsonAgent.addPair("Username", resultSet.getString("userName"));
            }

            // echo result
            MRDS.send(outputStream, myJsonAgent.getJson());
        } catch (SQLException e) {
            // failed signing up
            Log.log("Mysql exception at Sign Up handler. " + e.getMessage());
            myJsonAgent.addPair("Result", "Fail");

            // echo failed imformation
            MRDS.send(outputStream, myJsonAgent.getJson());
        }

    }


    /**
     * Look up user stats
     * @param id user id
     * @param field stat to look up (games won, games play, score)
     */
    public void requestStatsHandler(String id, String field){

        // building query
        String query = "select " + field + " from stats where userid = " + Integer.parseInt(id) + ";";

        // format message to be sent to client as Json
        MyJsonAgent myJsonAgent = new MyJsonAgent("FetchDataResult");
        myJsonAgent.addPair("field", field);

        try {
            // fetch data
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                myJsonAgent.addPair("Result", "Success");
                myJsonAgent.addPair("value", resultSet.getInt(field));
            }

            // echo result
            MRDS.send(outputStream, myJsonAgent.getJson());

        } catch (java.sql.SQLException e) {
            // failed
            Log.log("Mysql exception at Request Data handler. " + e.getMessage());
            myJsonAgent.addPair("Result", "Fail");
            MRDS.send(outputStream, myJsonAgent.getJson());
        }

    }


    /**
     * Update user stats
     * @param id user id
     * @param field stat to update
     * @param value values to change (add to deduce score, etc.)
     */
    public void updateStatsHandler(String id, String field, String value){

        // Logging in query
        String query = "update stats set " + field + " = " + field + " + " + value + " where userid = " + id + ";";

        // format message to be sent to client as Json
        MyJsonAgent myJsonAgent = new MyJsonAgent("UpdateStatsResult");
        myJsonAgent.addPair("field", field);

        try {
            // fetch data
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
            myJsonAgent.addPair("Result", "Success");
            // echo result
            MRDS.send(outputStream, myJsonAgent.getJson());

        } catch (java.sql.SQLException e) {
            // failed
            Log.log("Mysql exception at Update Stats handler. " + e.getMessage());
            myJsonAgent.addPair("Result", "Fail");
            MRDS.send(outputStream, myJsonAgent.getJson());
        }
    }
}
