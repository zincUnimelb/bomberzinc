import com.sun.tools.javah.Util;

import java.nio.channels.SocketChannel;
import java.sql.*;
import java.net.*;
import java.io.*;

public class Main {

    public static int portNumber = 3333;

    public static void main(String[] args) {

        // statement to excute query for DB manipulations
        Statement statement = null;

        Connection connection = null;


        //region Start listening on port
        while (true) {
            try (

                    // create new socket
                    ServerSocket serverSocket = new ServerSocket(portNumber);
                    // start listening on portNumber
                    Socket clientSocket = serverSocket.accept();

                    // get input and output stream
                    InputStream in = clientSocket.getInputStream();
                    OutputStream out = clientSocket.getOutputStream();
            ) {
                connection = getConnection();
                QueryHandler queryHandler = new QueryHandler(connection, out);
                // receiving data
                String receivedMessage = MRDS.receive(in);
                Log.log(receivedMessage);
                queryHandler.parseQuery(receivedMessage);
                try {
                    connection.close();
                } catch (SQLException e) {
                    Log.log("Exception at Connection close. " + e.getMessage());
                }

            } catch (IOException e) {
                String message = "Exception caught when trying to listen on port "
                        + portNumber + " or listening for a connection";
                System.err.println(message);
                System.err.println(e.getMessage());
                Log.log("Exception caught when trying to listen on port "
                        + portNumber + " or listening for a connection. " + e.getMessage());
                System.exit(1);
            }


//            System.out.println("Connection lost.");
        }
        //endregion

    }

    private static Connection getConnection() {

        //region Connect to DataBase
        String myDBUser = "root";
//        String myDBPW = "lawrence1321";
        String myDBPW = "ZINC"; // use this line when deploying on server
        String hostName = "localhost";
        String databaseName = "BomberZinc";

        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(
                    "jdbc:mysql://"+hostName+":3306/"+databaseName, myDBUser, myDBPW);
//            statement = connection.createStatement();
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }


        if (connection != null) {

//            System.out.println("Successfully connectted to MySQL");
        } else {
            System.exit(1);
        }
        //endregion
        return connection;
    }

}
