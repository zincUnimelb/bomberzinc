### README ###

## Accounts for test ##
0. Email PASSWORD
1. caibi@gmail.com 111111
2. caibi@gmail.com 111111

## Environment Setup: ##

Install SourceTree, and clone this repository to your local computer.

Install Unity! [Download here](https://store.unity.com/download?ref=personal) Current Version 5.4.2f

Open the cloned project in Unity, it should be in C:\\Users\\user\\Documents\\bomberzinc, or MyDocuments\\bomberzinc

Make sure when you make changes, you commit (which changes only your local git), then push (to publish changes to the repository). Press pull to update your local git with the bitbucket shared git.

Also, make sure to close Unity before committing and pushing in SourceTree if possible!

## Unit Testing: ##

I've already included the package from the Asset Store for UnityTestTools in this repo, here's some good documentation on it:

https://bitbucket.org/Unity-Technologies/unitytesttools/wiki/Home

Testing scnes are located in /Tests. To run the tests bring up the test panel with CTRL-ALT-SHIFT-T, and press run all tests. 
To create a new test press create test and make all necassary testing components a child of the created test. Edit your test parameters here.

Tests for JSON format generator, account system and server connection are located at */BomberZincUnityProject/Assets/Editor/*